const viewHandlerFactory = ({
  handleGetUsers
}) => {

  return {
    userViewHandler: async (req, res) => {
      const userList = (await handleGetUsers()).body;
      res.locals = { userList };
      res.render('users.njk');
    }
  };

};


module.exports = viewHandlerFactory;
