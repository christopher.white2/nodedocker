const factory = ({ getAllUsers, getUser }) => {
  const handleGetUsers = async () => {
    console.log("get Users");
    return {
      body: await getAllUsers()
    };
  };

  const handleGetUser = async ({user}) => {
    console.log("get User");
    return {
      body: await getUser(user)
    };
  };

  return {
    handleGetUser,
    handleGetUsers
  };

};



module.exports = factory;
