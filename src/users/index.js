const client = require('./client');

const handlerFactory = require('./controller');

module.exports = handlerFactory(client);
