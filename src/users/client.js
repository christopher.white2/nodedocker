const fetch = require('node-fetch');

const getAllUsers = () => {
  return fetch('http://jsonplaceholder.typicode.com/users')
    .then(response => response.json());
};

const getUser = (id) => {
  return fetch(`http://jsonplaceholder.typicode.com/users/${id}`)
    .then(response => response.json());
};

module.exports = {
  getAllUsers, getUser
};
