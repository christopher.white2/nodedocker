const express = require('express');
const nunjucks = require('nunjucks');
const adapt = require('./expressAdapter');
const users = require('./users');
const { getUsers } = require('./databaseUsers/users');

const viewHandlerFactory = require('./viewHandlers');
const app = express();

// const { userViewHandler } = viewHandlerFactory(users);
const {userViewHandler} = viewHandlerFactory({
  handleGetUsers: getUsers
});

nunjucks.configure('src/views', {
  autoescape: true,
  express: app
});

app.get('/users.html', userViewHandler);


app.get('/', adapt(users.handleGetUsers));
app.get('/:user', adapt(users.handleGetUser));


module.exports = {
  start: (port = 3000) => {
    app.listen(port, () => {
      console.log(`Application listening on port ${port}`);
    });
  }
};
