const expressHandler = (handler) => {
  return async (req, res) => {
    const response = await handler(req.params);
    res.send(response.body);
  };
};

module.exports = expressHandler;
