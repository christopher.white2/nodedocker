const { expect } = require('chai');
const viewHandlerFactory = require('../src/viewHandlers');

describe('user view handler', () => {

  it('should render the user template', async () => {

    const { userViewHandler } = viewHandlerFactory({
      handleGetUsers: () => []
    });

    let templateCalled;
    const res = {
      render: (template) => templateCalled = template,
    };

    await userViewHandler({}, res);

    expect(templateCalled).to.equal('users.njk');
  });

});
