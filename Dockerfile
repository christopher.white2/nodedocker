FROM node:16

RUN mkdir /app

WORKDIR /app

COPY package.json /app/
COPY package-lock.json /app/

COPY src /app/src

RUN npm install

ENTRYPOINT ["npm", "run", "start"]

